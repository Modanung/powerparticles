/* Power Particles
// Copyright (C) 2021 LucKey Productions (luckeyproductions.nl)
//
// This program is free software; you can redistribute it and/or modify
// it under the terms of the GNU General Public License as published by
// the Free Software Foundation; either version 2 of the License, or
// (at your option) any later version.
//
// This program is distributed in the hope that it will be useful,
// but WITHOUT ANY WARRANTY; without even the implied warranty of
// MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
// GNU General Public License for more details.
//
// You should have received a copy of the GNU General Public License along
// with this program; if not, write to the Free Software Foundation, Inc.,
// 51 Franklin Street, Fifth Floor, Boston, MA 02110-1301 USA.
*/

#include "mastercontrol.h"

DRY_DEFINE_APPLICATION_MAIN(MasterControl);

MasterControl::MasterControl(Context* context): Application(context),
    particleGenerator_{}
{
//    particleGenerator_.color_.SetHarmonic(true);
}

void MasterControl::Setup()
{
    SetRandomSeed(TIME->GetSystemTime());

    engineParameters_[EP_LOG_NAME] = GetSubsystem<FileSystem>()->GetAppPreferencesDir("luckey", "logs") + "powerparticles.log";
    engineParameters_[EP_WINDOW_TITLE] = "Power Particles";
    engineParameters_[EP_WINDOW_ICON] = "icon.png";
    engineParameters_[EP_WORKER_THREADS] = false;
    engineParameters_[EP_RESOURCE_PATHS] = "Data;CoreData;";

//    engineParameters_[EP_FULL_SCREEN] = false;
//    engineParameters_[EP_WINDOW_WIDTH]  = 800;
//    engineParameters_[EP_WINDOW_HEIGHT] = 600;


    SubscribeToEvent(E_UPDATE, DRY_HANDLER(MasterControl, HandleUpdate));
    SubscribeToEvent(E_POSTRENDERUPDATE, DRY_HANDLER(MasterControl, HandlePostRenderUpdate));
}
void MasterControl::Start()
{
    if (GRAPHICS)
        ENGINE->SetMaxFps(GRAPHICS->GetRefreshRate());


    context_->RegisterFactory<FireOrb>();

    CreateScene();
}
void MasterControl::Stop()
{
    engine_->DumpResources(true);
}
void MasterControl::Exit()
{
    engine_->Exit();
}

void MasterControl::CreateScene()
{
    Zone* zone{ RENDERER->GetDefaultZone() };
    zone->SetAmbientColor(Color::WHITE * 0.25f);
//    zone->SetFogColor(Color::WHITE);

    scene_ = new Scene(context_);
    scene_->CreateComponent<Octree>();
    scene_->CreateComponent<PhysicsWorld>();
    scene_->CreateComponent<DebugRenderer>();

//    for (int i{ 0 }; i < 55; ++i)
//        scene_->CreateChild("Orb")->CreateComponent<FireOrb>();

    //Light
    Node* lightNode{ scene_->CreateChild("Light") };
    lightNode->SetPosition(Vector3(2.0f, 3.0f, 5.0f) * 5.f);
    Light* light{ lightNode->CreateComponent<Light>() };
    light->SetBrightness(1.3f);
    light->SetRange(55.f);
    //Camera
    Node* cameraNode{ scene_->CreateChild("Camera") };
    cameraNode->SetPosition(Vector3::ONE * 2.7f);
    cameraNode->LookAt(Vector3::ZERO);
    Camera* camera{ cameraNode->CreateComponent<Camera>() };
    RENDERER->SetViewport(0, new Viewport(context_, scene_, camera));
    camera->SetFov(111.f);
    camera->SetFarClip(500.f);


    RandomizePoly();
}

void MasterControl::HandleUpdate(StringHash eventType, VariantMap& eventData)
{ if (INPUT->GetKeyPress(KEY_ESCAPE)) Exit();

    const float dt{ eventData[Update::P_TIMESTEP].GetFloat() };


/*
    Color col{ colonomial_.Solve(t) };
    col.FromHSV(col.r_, col.g_, col.b_, col.a_);
    boxMaterial_->SetShaderParameter("MatDiffColor", col);

    Vector3 pos{ posinomial_.Solve(t) };
    scene_->GetChild("Sphere")->SetPosition(pos);
 scene_->GetChild("Sphere")->SetScale(col.Data()[3] * 5.f);
*/

    if (sinceSpawn_ >= spawnInterval_ && particles_.Size() < 8e3)
    {
        particles_.Push(particleGenerator_.RandomParticle());
//    particles_.Push(particleGenerator_.LerpedParticle(Cycle(scene_->GetElapsedTime() * .5f, -2.f, 3.f)));
        sinceSpawn_ = 0.f;


    }


    sinceSpawn_ += dt;
    for (PowerParticle* p: particles_)
    {
        p->age_ += dt;

        if (p->age_ > lifeExpectancy_)
        {
            delete p;
            particles_.Remove(p);
        }
    }

    if (scene_->GetElapsedTime() > 4.f)
    {
        RandomizePoly();

        scene_->SetElapsedTime(0.f);
        spawnRate = Random(1, 5);
    }


}

void MasterControl::HandlePostRenderUpdate(StringHash eventType, VariantMap& eventData)
{
    DebugRenderer* debug{ scene_->GetComponent<DebugRenderer>() };
    for (const PowerParticle* p: particles_)
    {
//        col.FromHSV(col.r_, col.g_, col.b_, col.a_);

//        const Vector3 normal{ (scene_->GetChild("Camera")->GetWorldPosition() - pos).Normalized() };

        const Vector3 scale{ p->GetScale() };
        debug->AddCircle(p->GetPosition(),
                         Quaternion{ p->GetRotation() } * Vector3::UP * scale.y_,
                         Max(scale.x_, scale.z_),
                         p->GetColor() * Min(1.0f, p->GetLinearVelocity().Length() * 10.f - p->GetAngularVelocity().Length() * 0.01f));
    }
}

void MasterControl::RandomizePoly()
{
    RandomizePosition();
    RandomizeRotation();
    RandomizeScale();
    RandomizeColor();
    RandomizeHarmonic();

    spawnInterval_ = Random(0.01f, .2f);
}

void MasterControl::RandomizePosition()
{
    Vector3 center{ RandomOffCenter(2.f),
                                    0.f ,
                    RandomOffCenter(3.f)};

    TypedPolynomial<Vector3> minPolynomial{ center + Vector3::DOWN };
    for (int i{ 1 }; i <= 4; ++i)
        minPolynomial.SetCoefficient(i, { RandomOffCenter(1.f),
                                          RandomOffCenter(1.f),
                                          RandomOffCenter(1.f) });

    minPolynomial.SetSlope({{ Random(), RandomOffCenter(.0125f, .075f) },
                            { Random(), RandomOffCenter(.0125f, .075f) },
                            { Random(), RandomOffCenter(.0125f, .075f) }});

    TypedPolynomial<Vector3> maxPolynomial{ center + Vector3::UP };
    for (int i{ 1 }; i <= 8; ++i)
        maxPolynomial.SetCoefficient(i, { RandomOffCenter(0.5f, 2.f / i),
                                          RandomOffCenter(0.5f, 2.f / i),
                                          RandomOffCenter(0.5f, 2.f / i) });

    maxPolynomial.SetSlope({{ Random(), RandomOffCenter(.0125f, .075f) },
                            { Random(), RandomOffCenter(.0125f, .075f) },
                            { Random(), RandomOffCenter(.0125f, .075f) }});

    TypedBipolynomial<Vector3> bipolynomial{ minPolynomial, maxPolynomial };
    particleGenerator_.position_ = bipolynomial;
}

void MasterControl::RandomizeRotation()
{
    Vector3 center{ RandomOffCenter(180.f),
                    RandomOffCenter(180.f),
                    RandomOffCenter(180.f)};

    TypedPolynomial<Vector3> minPolynomial{ center + Vector3::DOWN };
    for (int i{ 1 }; i <= 2; ++i)
        minPolynomial.SetCoefficient(i, { RandomOffCenter(180.f),
                                          RandomOffCenter(180.f),
                                          RandomOffCenter(180.f) });

    minPolynomial.SetSlope({{ Random(), RandomOffCenter(.125f, .75f) },
                            { Random(), RandomOffCenter(.125f, .75f) },
                            { Random(), RandomOffCenter(.125f, .75f) }});

    TypedPolynomial<Vector3> maxPolynomial{ center + Vector3::UP };
    for (int i{ 1 }; i <= 8; ++i)
        maxPolynomial.SetCoefficient(i, { RandomOffCenter(1.f / i),
                                          RandomOffCenter(1.f / i),
                                          RandomOffCenter(1.f / i) });

    maxPolynomial.SetSlope({{ Random(), RandomOffCenter(.125f, .75f) },
                            { Random(), RandomOffCenter(.125f, .75f) },
                            { Random(), RandomOffCenter(.125f, .75f) }});

    TypedBipolynomial<Vector3> bipolynomial{ minPolynomial, maxPolynomial };
    particleGenerator_.rotation_ = bipolynomial;
}

void MasterControl::RandomizeScale()
{
    Vector3 center{ Vector3::ONE * Random(.25f, 0.5f)};

    TypedPolynomial<Vector3> minPolynomial{ center + Vector3::DOWN };
    for (int i{ 1 }; i <= Random(2); ++i)
        minPolynomial.SetCoefficient(i, { RandomOffCenter(.1f),
                                          RandomOffCenter(.1f),
                                          RandomOffCenter(.1f) });

    minPolynomial.SetSlope({{ Random(), RandomOffCenter(.125f, .75f) },
                            { Random(), RandomOffCenter(.125f, .75f) },
                            { Random(), RandomOffCenter(.125f, .75f) }});

    TypedPolynomial<Vector3> maxPolynomial{ center + Vector3::UP };
    for (int i{ 1 }; i <= Random(4); ++i)
        maxPolynomial.SetCoefficient(i, { RandomOffCenter(.05f / i),
                                          RandomOffCenter(.05f / i),
                                          RandomOffCenter(.05f / i) });

    maxPolynomial.SetSlope({{ Random(), RandomOffCenter(.125f, .75f) },
                            { Random(), RandomOffCenter(.125f, .75f) },
                            { Random(), RandomOffCenter(.125f, .75f) }});

    TypedBipolynomial<Vector3> bipolynomial{ minPolynomial, maxPolynomial };
    particleGenerator_.scale_ = bipolynomial;
}

void MasterControl::RandomizeColor()
{
    Color center{ Random(), Random(), Random(.5f, 1.f) };

    TypedPolynomial<Color> minPolynomial{ center };
    for (int i{ 1 }; i <= 4; ++i)
        minPolynomial.SetCoefficient(i, { RandomOffCenter(0.1f),
                                          RandomOffCenter(0.1f),
                                          RandomOffCenter(0.1f) });

    minPolynomial.SetSlope({{ Random(), RandomOffCenter(.125f, .75f) },
                            { Random(), RandomOffCenter(.125f, .75f) },
                            { Random(), RandomOffCenter(.125f, .75f) }});

    TypedPolynomial<Color> maxPolynomial{ center };
    for (int i{ 1 }; i <= 8; ++i)
        maxPolynomial.SetCoefficient(i, { RandomOffCenter(0.125f / i),
                                          RandomOffCenter(0.125f / i),
                                          RandomOffCenter(0.125f / i) });

    maxPolynomial.SetSlope({{ RandomOffCenter(), RandomOffCenter(.125f, 1.5f) },
                            { RandomOffCenter(), RandomOffCenter(.125f, 1.5f) },
                            { RandomOffCenter(), RandomOffCenter(.125f, 1.5f) }});

    TypedBipolynomial<Color> bipolynomial{ minPolynomial, maxPolynomial };
    particleGenerator_.color_ = bipolynomial;
}

void MasterControl::RandomizeHarmonic()
{
    for (int i{ 0 }; i < 3; ++i)
        particleGenerator_.position_.SetPolynomialType(i, static_cast<PolynomialType>(Random(3)));
    for (int i{ 0 }; i < 3; ++i)
        particleGenerator_.rotation_.SetPolynomialType(i, static_cast<PolynomialType>(Random(3)));
    for (int i{ 0 }; i < 3; ++i)
        particleGenerator_.scale_.SetPolynomialType(i, static_cast<PolynomialType>(Random(3)));
    for (int i{ 0 }; i < 4; ++i)
        particleGenerator_.color_.SetPolynomialType(i, static_cast<PolynomialType>(Random(3)));
}
