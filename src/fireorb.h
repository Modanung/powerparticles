
#ifndef FIREORB_H
#define FIREORB_H

#include "luckey.h"
#include "Dry/Math/Polynomial.h"


class FireOrb: public LogicComponent
{
    DRY_OBJECT(FireOrb, LogicComponent);

public:
    FireOrb(Context* context, float randomizer = Random());
//    void OnSetEnabled() override;
//    void Start() override;
//    void DelayedStart() override;
//    void Stop() override;

    void Update(float timeStep) override;
    void PostUpdate(float timeStep) override;
    void FixedUpdate(float timeStep) override;
    void FixedPostUpdate(float timeStep) override;

    bool Save(Serializer& dest) const override;
    bool SaveXML(XMLElement& dest) const override;
    bool SaveJSON(JSONValue& dest) const override;
    void MarkNetworkUpdate() override;
    void GetDependencyNodes(PODVector<Node*>& dest) override;
    void DrawDebugGeometry(DebugRenderer* debug, bool depthTest) override;

protected:
    void OnNodeSet(Node* node) override;
    void OnSceneSet(Scene* scene) override;
    void OnMarkedDirty(Node* node) override;
    void OnNodeSetEnabled(Node* node) override;

    TypedPolynomial<Vector3> test_;

    float randomizer_;

private:
    static StaticModelGroup* orbs_;
};

#endif // FIREORB_H
