
#include "fireorb.h"

StaticModelGroup* FireOrb::orbs_{ nullptr };

FireOrb::FireOrb(Context* context, float randomizer): LogicComponent(context),
    randomizer_{ randomizer }
{
    TypedBipolynomial<Vector3> source{
        {{
        { { .2f,  0.5f,  1.5f      }, { 1.f, .1f, }, PT_HARMONIC_SIN },
        { { .2f, -.2f, 1.5f, -0.5f }, { .5f, .2f, }, PT_HARMONIC_SIN },
        { { .2f,  0.5f,  1.5f      },  { 1.f, .1f, }, PT_HARMONIC_COS }
        }},
        {{
        { { 0.f, 1.f,  1.f },        { 0.f, .3f, }, PT_HARMONIC_SIN },
        { { 0.f            },        { -.5f, -.2f, }, PT_HARMONIC_SIN },
        { { 0.f, 1.f,  1.f },        { 0.f, .3f, }, PT_HARMONIC_COS }
        }}
    };

    test_ = source.ExtractRandom();
}

//void FireOrb::OnSetEnabled() { LogicComponent::OnSetEnabled(); }
//void FireOrb::Start() {}
//void FireOrb::DelayedStart() {}
//void FireOrb::Stop() {}
void FireOrb::Update(float timeStep)
{
    Vector3 pos{ test_.Solve(GetScene()->GetElapsedTime()) };
    node_->SetPosition(pos);
    node_->SetScale(.75f - 0.5f * test_.Derived().Solve(GetScene()->GetElapsedTime()).Length());
}
void FireOrb::PostUpdate(float timeStep) {}
void FireOrb::FixedUpdate(float timeStep) {}
void FireOrb::FixedPostUpdate(float timeStep) {}
void FireOrb::OnNodeSet(Node* node)
{
    node_->SetRotation(Quaternion{ Random(360.f), Random(360.f), Random(360.f) });

    if (!orbs_)
    {
        orbs_ = GetScene()->CreateComponent<StaticModelGroup>();
        orbs_->SetModel(RES(Model, "Models/Sphere.mdl"));
        orbs_->SetMaterial(RES(Material, "Materials/Fire.xml"));
    }

    orbs_->AddInstanceNode(node_);
}
void FireOrb::OnSceneSet(Scene* scene) { LogicComponent::OnSceneSet(scene); }
void FireOrb::OnMarkedDirty(Node* node) {}
void FireOrb::OnNodeSetEnabled(Node* node) {}
bool FireOrb::Save(Serializer& dest) const { return LogicComponent::Save(dest); }
bool FireOrb::SaveXML(XMLElement& dest) const { return LogicComponent::SaveXML(dest); }
bool FireOrb::SaveJSON(JSONValue& dest) const { return LogicComponent::SaveJSON(dest); }
void FireOrb::MarkNetworkUpdate() { LogicComponent::MarkNetworkUpdate(); }
void FireOrb::GetDependencyNodes(PODVector<Node*>& dest) {}
void FireOrb::DrawDebugGeometry(DebugRenderer* debug, bool depthTest) {}
