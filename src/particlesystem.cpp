/* Power Particles
// Copyright (C) 2021 LucKey Productions (luckeyproductions.nl)
//
// This program is free software; you can redistribute it and/or modify
// it under the terms of the GNU General Public License as published by
// the Free Software Foundation; either version 2 of the License, or
// (at your option) any later version.
//
// This program is distributed in the hope that it will be useful,
// but WITHOUT ANY WARRANTY; without even the implied warranty of
// MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
// GNU General Public License for more details.
//
// You should have received a copy of the GNU General Public License along
// with this program; if not, write to the Free Software Foundation, Inc.,
// 51 Franklin Street, Fifth Floor, Boston, MA 02110-1301 USA.
*/

#include "particlesystem.h"

ParticleSystem::ParticleSystem(Context* context): Component(context)
{
}

void ParticleSystem::OnSetEnabled() { Component::OnSetEnabled(); }
void ParticleSystem::OnNodeSet(Node* node) {}
void ParticleSystem::OnSceneSet(Scene* scene) {}
void ParticleSystem::OnMarkedDirty(Node* node) {}
void ParticleSystem::OnNodeSetEnabled(Node* node) {}
bool ParticleSystem::Save(Serializer& dest) const { return Component::Save(dest); }
bool ParticleSystem::SaveXML(XMLElement& dest) const { return Component::SaveXML(dest); }
bool ParticleSystem::SaveJSON(JSONValue& dest) const { return Component::SaveJSON(dest); }
void ParticleSystem::MarkNetworkUpdate() { Component::MarkNetworkUpdate(); }
void ParticleSystem::GetDependencyNodes(PODVector<Node*>& dest) {}
void ParticleSystem::DrawDebugGeometry(DebugRenderer* debug, bool depthTest) {}
