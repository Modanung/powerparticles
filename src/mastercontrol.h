/* Power Particles
// Copyright (C) 2021 LucKey Productions (luckeyproductions.nl)
//
// This program is free software; you can redistribute it and/or modify
// it under the terms of the GNU General Public License as published by
// the Free Software Foundation; either version 2 of the License, or
// (at your option) any later version.
//
// This program is distributed in the hope that it will be useful,
// but WITHOUT ANY WARRANTY; without even the implied warranty of
// MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
// GNU General Public License for more details.
//
// You should have received a copy of the GNU General Public License along
// with this program; if not, write to the Free Software Foundation, Inc.,
// 51 Franklin Street, Fifth Floor, Boston, MA 02110-1301 USA.
*/

#ifndef MASTERCONTROL_H
#define MASTERCONTROL_H

#include "luckey.h"

#include "particlesystem.h"
#include "fireorb.h"

namespace Dry {
class Node;
class Scene;
}


class MasterControl : public Application
{
    DRY_OBJECT(MasterControl, Application);
public:
    MasterControl(Context* context);

    Scene* GetScene() const { return scene_; }

    // Setup before engine initialization. Modifies the engine paramaters.
    virtual void Setup();
    // Setup after engine initialization.
    virtual void Start();
    // Cleanup after the main loop. Called by Application.
    virtual void Stop();
    void Exit();

    template <class T> void RegisterObject() { context_->RegisterFactory<T>(); }
    template <class T> void RegisterSubsystem() { context_->RegisterSubsystem(new T(context_)); }

    void HandleUpdate(StringHash eventType, VariantMap &eventData);
    void HandlePostRenderUpdate(StringHash eventType, VariantMap &eventData);

private:
    void CreateScene();

    Scene* scene_;
//    StaticModelGroup* fireOrbs_;

//    TypedPolynomial<Vector3> test_;

    ParticleGenerator particleGenerator_;
    PODVector<PowerParticle*> particles_;

    float sinceSpawn_{ M_INFINITY };
    float spawnInterval_{ .075f };
    int spawnRate{ 1 };
    const float lifeExpectancy_{ 5.f };


    void RandomizePoly();
    void RandomizeHarmonic();
    void RandomizePosition();
    void RandomizeRotation();
    void RandomizeScale();
    void RandomizeColor();
};

#endif // MASTERCONTROL_H
