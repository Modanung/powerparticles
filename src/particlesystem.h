/* Power Particles
// Copyright (C) 2021 LucKey Productions (luckeyproductions.nl)
//
// This program is free software; you can redistribute it and/or modify
// it under the terms of the GNU General Public License as published by
// the Free Software Foundation; either version 2 of the License, or
// (at your option) any later version.
//
// This program is distributed in the hope that it will be useful,
// but WITHOUT ANY WARRANTY; without even the implied warranty of
// MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
// GNU General Public License for more details.
//
// You should have received a copy of the GNU General Public License along
// with this program; if not, write to the Free Software Foundation, Inc.,
// 51 Franklin Street, Fifth Floor, Boston, MA 02110-1301 USA.
*/

#ifndef PARTICLESYSTEM_H
#define PARTICLESYSTEM_H

#include "luckey.h"
#include "Dry/Math/Polynomial.h"

struct PowerParticle
{
    enum Property{ PAR_POSITION = 0, PAR_ROTATION, PAR_SCALE, PAR_COLOR, PAR_AGE };

    PowerParticle(const TypedPolynomial<Vector3>& position, const TypedPolynomial<Vector3>& rotation,
                  const TypedPolynomial<Vector3>& scale,    const TypedPolynomial<Color>& color):
        position_{ position },
        rotation_{ rotation },
        scale_{ scale },
        color_{ color },
        age_{ 0.f }
    {
    }

    void Update(float timeStep)
    {
        age_ += timeStep;
        requireUpdate_ = true;
    }

    void DoUpdate()
    {

    }

    Matrix3x4 GetTransform() const
    {
        return Matrix3x4{ GetPosition(),
                          Quaternion{ GetRotation() },
                          GetScale() };
    }

    Vector3 GetLinearVelocity()    const { return position_.Derived().Solve(age_); }
    Vector3 GetAngularVelocity()   const { return rotation_.Derived().Solve(age_); }
    Vector3 GetScalarVelocity()    const { return scale_.Derived().Solve(age_);    }
    Color   GetChromaticVelocity() const { return color_.Derived().Solve(age_);    }

    Vector3 GetPosition() const { return position_.Solve(age_); }
    Vector3 GetRotation() const { return rotation_.Solve(age_); }
    Vector3 GetScale()    const { return scale_.Solve(age_);    }
    Color   GetColor()    const { return color_.Solve(age_);    }

    TypedPolynomial<Vector3> position_;
    TypedPolynomial<Vector3> rotation_;
    TypedPolynomial<Vector3> scale_;
    TypedPolynomial<Color>   color_;


    bool requireUpdate_{ true };
    float age_;
    Matrix3x4 transform_;
    Matrix3x4 motion_;
};

struct ParticleGenerator
{
    ParticleGenerator(const TypedBipolynomial<Vector3>& position, const TypedBipolynomial<Vector3>& rotation,
                      const TypedBipolynomial<Vector3>& scale,    const TypedBipolynomial<Color>& color):
        position_{ position },
        rotation_{ rotation },
        scale_{ scale },
        color_{ color }
    {
    }

    ParticleGenerator():
        position_{ Vector3::ZERO },
        rotation_{ Vector3::ZERO },
        scale_{ Vector3::ONE },
        color_{ Color::GRAY }
    {
    }

    PowerParticle* RandomParticle()
    {
        return new PowerParticle{
                 position_.ExtractRandom(),
                 rotation_.ExtractRandom(),
                 scale_.ExtractRandom(),
                 color_.ExtractRandom() };
    }

    PowerParticle* LerpedParticle(float t)
    {
        return new PowerParticle{
                 position_.ExtractLerped(t),
                 rotation_.ExtractLerped(t),
                 scale_.ExtractLerped(t),
                 color_.ExtractLerped(t) };
    }

    TypedBipolynomial<Vector3> position_;
    TypedBipolynomial<Vector3> rotation_;
    TypedBipolynomial<Vector3> scale_;
    TypedBipolynomial<Color> color_;
};

struct DataMask
{
    DataMask():
        clip_{}
    {}

    bool Masked(const PODVector<float>& data)
    {
        for (unsigned i{ 0u }; i < data.Size(); ++i)
        {
            if (clip_.Size() > i)
            {
                const float val{ data.At(i) };
                const Vector2 range{ clip_.At(i) };

                if (val < range.x_ || val > range.y_)
                    return true;
            }
            else break;
        }

        return false;
    }

    Vector<Vector2> clip_;
};

struct Trimmer
{
    using MaskType = Pair<PowerParticle::Property, bool>;

//    bool Snip(PowerParticle* particle) const
//    {
//        for (const MaskType pd: limits_.Keys())
//        {
//            const PowerParticle::Property property{ pd.first_ };
//            const bool derivative{ pd.second_ };
//            const DataMask& mask{ *limits_[pd] };

//            if (property == PowerParticle::Property::PAR_COLOR)
//            {
//                TypedPolynomial<Color> polynomial{ particle->color_ };
//            }
//            else
//            {
//                TypedPolynomial<Vector3> polynomial;

//                switch (property)
//                {
//                default:
//                    continue;
//                case PowerParticle::Property::PAR_POSITION:
//                    polynomial =
//                            /*derivative ? particle->position_.Derived() : */particle->position_;
//                    break;
//                case PowerParticle::Property::PAR_ROTATION:
//                    polynomial = particle->rotation_;
//                    break;
//                case PowerParticle::Property::PAR_SCALE:
//                    polynomial = particle->scale_;
//                    break;
//                }
//            }
//        }
//    }

    HashMap<MaskType, DataMask> limits_;
};

struct ParticleModifier
{
    virtual void Modify(PODVector<PowerParticle*>& particles) const = 0;
};

/// Removes particles
struct Pruner: public ParticleModifier
{
    Pruner() = default;

    void Modify(PODVector<PowerParticle*>& particles) const override
    {
        for (PowerParticle* p: particles)
        {
//            if (trimmer_.Snip(p))
//            {
//                delete p;
//                particles.Remove(p);
//            }
        }
    }

    Trimmer trimmer_{};
};

/// Replaces particles.
struct Scintillator: public ParticleGenerator, public ParticleModifier
{
    Scintillator() = default;

    void Modify(PODVector<PowerParticle*>& particles) const override
    {
        for (PowerParticle* p: particles)
        {
//            if (trimmer_.Snip(p))
//            {
                // Scintillate



//                delete p;
//                particles.Remove(p);
//            }
        }
    }

    IntVector2 scintillation_{ 5, 5 };
    Trimmer trimmer_{};
};

struct Magnet
{
    Quaternion rotation_;
    Vector3 pull_;
    Vector<Vector2> falloff_;
    float vorticity_;
};

struct Knot
{
    Matrix3x4 transform_;
    float weight_;
};

typedef Vector<PODVector<Knot> > Emissary;

class ParticleSystem: public Component
{
    DRY_OBJECT(ParticleSystem, Component);

public:
    ParticleSystem(Context* context);
    void OnSetEnabled() override;

    bool Save(Serializer& dest) const override;
    bool SaveXML(XMLElement& dest) const override;
    bool SaveJSON(JSONValue& dest) const override;
    void MarkNetworkUpdate() override;
    void GetDependencyNodes(PODVector<Node*>& dest) override;
    void DrawDebugGeometry(DebugRenderer* debug, bool depthTest) override;

    void Trigger()
    {
        elapsedTime_ = 0.f;
    }

protected:
    void OnNodeSet(Node* node) override;
    void OnSceneSet(Scene* scene) override;
    void OnMarkedDirty(Node* node) override;
    void OnNodeSetEnabled(Node* node) override;

private:
    bool emitting_;
    float elapsedTime_;
    PODVector<PowerParticle*> particles_;
    Emissary emissary_;

//    Vector<Magnet> magnets_;
//    Vector3 gravity_;
};

#endif // PARTICLESYSTEM_H
