HEADERS += \
    $$PWD/fireorb.h \
    src/luckey.h \
#    src/polynomial.h \
    src/mastercontrol.h \
    src/particlesystem.h \

SOURCES += \
    $$PWD/fireorb.cpp \
    src/luckey.cpp \
    src/mastercontrol.cpp \
    src/particlesystem.cpp \
